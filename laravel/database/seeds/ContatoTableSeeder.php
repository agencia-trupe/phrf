<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email'       => 'contato@trupe.net',
            'telefones'   => '+55 11 3208·3851<br>+55 11 3271·5968',
            'endereco'    => 'Av. Brigadeiro Luis Antônio, 4486 · Jd. Paulista<br>01402-002 · São Paulo SP',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.650935610997!2d-46.66738124885133!3d-23.58097706812811!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59e6cf9f54d3%3A0x9733512268cf3d02!2sAv.+Brigadeiro+Lu%C3%ADs+Ant%C3%B4nio%2C+4486+-+Jardim+Paulista%2C+S%C3%A3o+Paulo+-+SP%2C+01402-002!5e0!3m2!1spt-BR!2sbr!4v1530278466647" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>'
        ]);
    }
}
