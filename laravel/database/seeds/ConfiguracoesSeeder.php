<?php

use Illuminate\Database\Seeder;

class ConfiguracoesSeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'nome_do_site'               => 'Paulo Hamilton e Reina Filho',
            'title'                      => 'Paulo Hamilton e Reina Filho &middot; Sociedade de Advogados',
            'description'                => '',
            'keywords'                   => '',
            'imagem_de_compartilhamento' => '',
            'analytics'                  => '',
        ]);
    }
}
