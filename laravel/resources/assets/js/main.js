import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.equipe-fancybox').fancybox({
    type: 'ajax',
    width: '100%',
    height: '100%',
    maxWidth: '1200px',
    fitToView: false,
    autoSize: false
});
