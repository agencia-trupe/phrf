@extends('frontend.common.template')

@section('content')

    <div class="areas-de-atuacao">
        <div class="center">
            <h2 class="titulo">Áreas de Atuação</h2>
            <div class="areas">
                @foreach($areas as $area)
                    <div>
                        <h3>{{ $area->titulo }}</h3>
                        {!! $area->texto !!}
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection