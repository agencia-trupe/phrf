@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="center">
            <h2 class="titulo">Contato</h2>
            <div class="informacoes">
                <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
                <div>
                    <p class="telefones">{!! $contato->telefones !!}</p>
                    <p class="endereco">{!! $contato->endereco !!}</p>
                </div>
            </div>
            <form action="{{ route('contato.post') }}" method="POST">
                {!! csrf_field() !!}

                @if(session('success'))
                   <div class="aviso enviado">Mensagem enviada com sucesso!</div>
                @endif

                @if($errors->any())
                    <div class="aviso erros">
                        @foreach($errors->all() as $error)
                            {!! $error !!}<br>
                        @endforeach
                    </div>
                @endif
                
                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}">
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}">
                <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                <textarea name="mensagem" placeholder="mensagem">{{ old('mensagem') }}</textarea>
                <input type="submit" value="ENVIAR">
            </form>
        </div>
        
        <div class="mapa">
            {!! $contato->google_maps !!}
        </div>
    </div>

@endsection
