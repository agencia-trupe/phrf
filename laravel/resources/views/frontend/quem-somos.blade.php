@extends('frontend.common.template')

@section('content')

    <div class="quem-somos">
        <div class="center">
            <div class="imagens">
                <img src="{{ asset('assets/img/quem-somos/'.$quemSomos->imagem_1) }}" alt="">
                <img src="{{ asset('assets/img/quem-somos/'.$quemSomos->imagem_2) }}" alt="">
                <img src="{{ asset('assets/img/quem-somos/'.$quemSomos->imagem_3) }}" alt="">
                <img src="{{ asset('assets/img/quem-somos/'.$quemSomos->imagem_4) }}" alt="">
                <img src="{{ asset('assets/img/quem-somos/'.$quemSomos->imagem_5) }}" alt="">
            </div>
            <div class="texto">
                {!! $quemSomos->texto !!}
            </div>
        </div>

        <div class="missao-valores">
            <div class="center">
                <div class="col">
                    <h3>Nossa Missão</h3>
                    <p>{!! $quemSomos->missao !!}</p>
                </div>
                <div class="col">
                    <h3>Nossos Valores</h3>
                    <p>{!! $quemSomos->valores !!}</p>
                </div>
            </div>
        </div>
    </div>

@endsection
