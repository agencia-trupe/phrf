@extends('frontend.common.template')

@section('content')

    <div class="equipe">
        <div class="center">
            <h2 class="titulo">Equipe</h2>
            
            <div class="lista">
                @foreach($equipe as $membro)
                    <a href="{{ route('equipe.show', $membro->id) }}" class="equipe-fancybox">
                        {{ $membro->nome }}
                        <span>{{ $membro->subtitulo }}</span>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection