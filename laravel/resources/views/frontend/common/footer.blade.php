    <footer>
        <div class="center">
            <nav>@include('frontend.common.nav')</nav>
            <div class="informacoes">
                <div class="marca">
                    <img src="{{ asset('assets/img/layout/marca-rodape.png') }}" alt="">
                </div>
                <div class="telefones">
                    {!! $contato->telefones !!}
                </div>
                <div class="endereco">
                    {!! $contato->endereco !!}
                </div>
                <div class="copyright">
                    © {{ date('Y') }} {{ $config->nome_do_site }} - Todos os direitos reservados.
                    <br>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </div>
            </div>
        </div>
    </footer>
