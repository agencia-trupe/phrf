<a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>home</a>
<a href="{{ route('quem-somos') }}" @if(Tools::isActive('quem-somos')) class="active" @endif>quem somos</a>
<a href="{{ route('equipe') }}" @if(Tools::isActive('equipe')) class="active" @endif>equipe</a>
<a href="{{ route('areas-de-atuacao') }}" @if(Tools::isActive('areas-de-atuacao')) class="active" @endif>áreas de atuação</a>
<a href="{{ route('noticias-e-artigos') }}" @if(Tools::isActive('noticias-e-artigos')) class="active" @endif>notícias e artigos</a>
<a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>contato</a>
