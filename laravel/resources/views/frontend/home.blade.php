@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="center">
            <div class="banner">
                <img src="{{ asset('assets/img/layout/banner-home.png') }}" alt="">
            </div>

            <div class="areas-de-atuacao">
                <h2>Nossas Áreas de Atuação</h2>
                <div>
                    @foreach($areasDeAtuacao as $area)
                    <a href="{{ route('areas-de-atuacao') }}">{{ $area->titulo }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
