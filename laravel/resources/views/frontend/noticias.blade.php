@extends('frontend.common.template')

@section('content')

    <div class="noticias-e-artigos">
        <div class="center">
            <h2 class="titulo">Notícias e Artigos</h2>

            <div class="noticia">
                <h1>{{ $noticia->titulo }}</h1>
                <p class="informacoes">
                    {{ Tools::formataData($noticia->data) }}
                    &middot;
                    {{ $noticia->autor }}
                </p>
                {!! $noticia->texto !!}
            </div>

            <div class="lista-noticias">
                @foreach($noticias as $titulo => $slug)
                    <a href="{{ route('noticias-e-artigos', $slug) }}" @if($slug == $noticia->slug) class="active" @endif>
                        {{ $titulo }}
                    </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection