@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'quemSomos']) !!}
</div>

<div class="form-group">
    {!! Form::label('missao', 'Missão') !!}
    {!! Form::textarea('missao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('valores', 'Valores') !!}
    {!! Form::textarea('valores', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1 (360x230px)') !!}
            @if($registro->imagem_1)
            <img src="{{ url('assets/img/quem-somos/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2 (230x230px)') !!}
            @if($registro->imagem_2)
            <img src="{{ url('assets/img/quem-somos/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem_3', 'Imagem 3 (600x230px)') !!}
    @if($registro->imagem_3)
    <img src="{{ url('assets/img/quem-somos/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_4', 'Imagem 4 (230x230px)') !!}
            @if($registro->imagem_4)
            <img src="{{ url('assets/img/quem-somos/'.$registro->imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_4', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_5', 'Imagem 5 (360x230px)') !!}
            @if($registro->imagem_5)
            <img src="{{ url('assets/img/quem-somos/'.$registro->imagem_5) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_5', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
