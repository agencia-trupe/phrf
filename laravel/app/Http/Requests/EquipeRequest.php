<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EquipeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'subtitulo' => 'required',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
