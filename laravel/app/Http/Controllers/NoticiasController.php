<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Noticia;

class NoticiasController extends Controller
{
    public function index(Noticia $noticia)
    {
        if (!$noticia->id) {
            $noticia = Noticia::ordenados()->firstOrFail();
        }

        $noticias = Noticia::ordenados()->lists('slug', 'titulo');

        return view('frontend.noticias', compact('noticias', 'noticia'));
    }
}
