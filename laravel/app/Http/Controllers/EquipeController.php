<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Membro;

class EquipeController extends Controller
{
    public function index()
    {
        $equipe = Membro::ordenados()->get();

        return view('frontend.equipe.index', compact('equipe'));
    }

    public function show(Request $request, $id)
    {
        $membro = Membro::findOrFail($id);

        return view('frontend.equipe.show', compact('membro'));
    }
}
