<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\AreaDeAtuacao;

class AreasDeAtuacaoController extends Controller
{
    public function index()
    {
        $areas = AreaDeAtuacao::ordenados()->get();

        return view('frontend.areas-de-atuacao', compact('areas'));
    }
}
