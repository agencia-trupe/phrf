<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\AreaDeAtuacao;

class HomeController extends Controller
{
    public function index()
    {
        $areasDeAtuacao = AreaDeAtuacao::ordenados()->get();

        return view('frontend.home', compact('areasDeAtuacao'));
    }
}
